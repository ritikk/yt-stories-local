from .base import CustomException


class NoValidData(CustomException):
    code = 400
    error_code = 30001
    message = "Incomplete request, " \
              "one or more of this parameters is required: "

    def __init__(self, **kwargs):
        if kwargs:
            for args in kwargs.keys():
                self.message = self.message + " " + str(args)
