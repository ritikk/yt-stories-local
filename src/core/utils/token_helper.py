from datetime import datetime, timedelta
from typing import NoReturn, Union

from config import config
from core.exceptions.token import DecodeTokenException, ExpiredTokenException
from fastapi import HTTPException
from fastapi.security import HTTPBearer
from fastapi.security.utils import get_authorization_scheme_param
from jose.exceptions import ExpiredSignatureError, JWTError
from jose.jwt import decode, encode
from starlette.requests import Request
from starlette.status import HTTP_403_FORBIDDEN


class TokenHelper(HTTPBearer):
    # @classmethod
    # def encode(cls, payload: dict,
    #            expire_period: int = config.JWT_EXPIRE_PERIOD
    #            ) -> str:
    #     token = encode(
    #         claims={
    #             **payload,
    #             "exp": datetime.utcnow() + timedelta(seconds=expire_period),
    #         },
    #         key=config.JWT_SECRET_KEY,
    #         algorithm=config.JWT_ALGORITHM,
    #     )
    #     return token

    @classmethod
    def decode(cls, token: str) -> Union[dict, NoReturn]:
        try:
            return decode(
                token=token,
                key=config.JWT_SECRET_KEY,
                algorithms=config.JWT_ALGORITHM,
            )
        except ExpiredSignatureError:
            raise ExpiredTokenException
        except JWTError:
            raise DecodeTokenException

    @classmethod
    def token_validate(cls, token: str):

        try:
            token = decode(
                token=token,
                key=config.JWT_SECRET_KEY,
                algorithms=config.JWT_ALGORITHM,
            )
            if token:
                return {'status': True}
            else:
                return {'status': False}
        except:

            return {'status': False}

    @staticmethod
    def decode_expired_token(token: str) -> Union[dict, NoReturn]:
        try:
            return decode(
                token,
                config.JWT_SECRET_KEY,
                config.JWT_ALGORITHM,
                options={"verify_exp": False},
            )
        except JWTError:
            raise DecodeTokenException

    async def __call__(
            self, request: Request
    ):
        authorization: str = request.headers.get("Authorization")
        scheme, credentials = get_authorization_scheme_param(authorization)
        if not (authorization and scheme and credentials):
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_403_FORBIDDEN, detail="Not authenticated"
                )
            else:
                return None
        if scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=HTTP_403_FORBIDDEN,
                    detail="Invalid authentication credentials",
                )
            else:
                return None

        return self.decode(token=credentials)
