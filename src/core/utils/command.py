from abc import ABC, abstractmethod
from datetime import datetime
from typing import NoReturn, Union

from fastapi_utils.api_model import APIModel

from core import Repo


class CommandService(ABC):

    @abstractmethod
    def __init__(self, **kwargs: Repo):

        pass

    @abstractmethod
    async def create(
            self, **kwargs: Union[str, float, int, bool, datetime]
    ) -> Union[APIModel, NoReturn]:

        pass

    @abstractmethod
    async def read(
            self, **kwargs: Union[str, float, int, bool, datetime]
    ) -> Union[APIModel, NoReturn]:

        pass

    @abstractmethod
    async def update(
            self, **kwargs: Union[str, float, int, bool, datetime]
    ) -> Union[APIModel, NoReturn]:

        pass

    @abstractmethod
    async def delete(
            self, **kwargs: Union[str, float, int, bool, datetime]
    ) -> None:

        pass
