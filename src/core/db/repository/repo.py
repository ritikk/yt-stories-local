from abc import ABC, abstractmethod
from typing import List, Union

from pydantic import BaseModel
from pydantic.schema import UUID


class Repo(ABC):

    @abstractmethod
    async def save(
            self, **kwargs: BaseModel
    ) -> BaseModel:
        pass

    @abstractmethod
    async def delete(
            self, **kwargs: BaseModel
    ) -> None:
        pass

    @abstractmethod
    async def get_by_id(
            self, id_: UUID
    ) -> Union[List[BaseModel], None]:
        pass

    @abstractmethod
    async def get_all(self) -> List[BaseModel]:
        pass
