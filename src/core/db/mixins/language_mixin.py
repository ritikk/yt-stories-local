from sqlalchemy import Column, String, func
from sqlalchemy.ext.declarative import declared_attr


class LanguageMixin:

    @declared_attr
    def language(cls):
        return Column(String, default=func.now(), nullable=False)
