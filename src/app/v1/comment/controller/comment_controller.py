from typing import List, NoReturn, Union

from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID
from core import ExceptionResponseSchema, TokenHelper

from ..schema import (CreateCommentRequest, CreateCommentResponse, Comment,
                      ReadCommentResponse, UpdateCommentRequest,
                      UpdateCommentResponse)
from ..service import CommentCommandService

router = InferringRouter()


@cbv(router=router)
class CommentController:

    @router.post(
        "/{post_id}/comment",
        response_model=CreateCommentResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Comment on a Post"
    )
    async def create_comment(
            self, post_id: UUID, request: CreateCommentRequest, payload: dict = Depends(dependency=TokenHelper())
    ) -> Union[Comment, NoReturn]:
        print(payload)
        return await CommentCommandService().create(payload=payload, post_id=post_id, **request.dict())

    @router.put(
        "/{post_id}/comment",
        include_in_schema=False,
        response_model=UpdateCommentResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Update Comment on a Post"
    )
    async def update_comment(
            self,
            request: UpdateCommentRequest, comment_id: UUID
    ) -> Union[Comment, NoReturn]:
        return await CommentCommandService(). \
            update(comment_id, **request.dict())

    @router.get(
        "/post/{post_id}/comment/{comment_id}",
        response_model=List[ReadCommentResponse],
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get all Comments of a Post"
    )
    async def get_all_comments(
            self,
    ) -> Union[List[Comment], NoReturn]:
        return await CommentCommandService().read(all_=True)

    @router.delete(
        "/post/{post_id}/comment/{comment_id}",
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Delete a Comment from a post"
    )
    async def delete_comment(
            self, post_id: UUID
    ) -> None:
        return await CommentCommandService().delete(id_=post_id)
