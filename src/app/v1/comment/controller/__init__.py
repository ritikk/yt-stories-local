from .comment_controller import CommentController
from .comment_controller import router as comment_router

__all__ = [
    "comment_router",
    "CommentController",
]
