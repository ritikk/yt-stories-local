from typing import List, Union

from pydantic.schema import UUID
from sqlalchemy import select

from core import Repo, session

from ..models import CommentModel


class CommentRepo(Repo):

    async def save(
            self,
            comment: CommentModel,
    ) -> CommentModel:
        session.add(comment)

        return comment

    async def delete(
            self, comment: CommentModel
    ) -> None:
        await session.delete(comment)

        return None

    async def get_by_id(
            self, id_: UUID
    ) -> Union[List[CommentModel], None]:
        query = await session.execute(
            select(CommentModel).filter(
                CommentModel.post_id == id_
            )
        )

        return query.scalars().all()

    async def get_all(self) -> List[CommentModel]:
        query = await session.execute(
            select(CommentModel)
        )

        return query.scalars().all()
