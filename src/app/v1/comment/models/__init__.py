from .comment_model import CommentModel

__all__ = [
    "CommentModel"
]
