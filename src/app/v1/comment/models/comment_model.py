import uuid
from typing import NoReturn, Union

from sqlalchemy import Boolean, Column, Float, Integer, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from core import Base, LanguageMixin, TimestampMixin

from ..schema import Comment


class CommentModel(Base, TimestampMixin):
    __tablename__ = "comment"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4())
    post_id = Column(UUID(as_uuid=True), ForeignKey("post.id"), default=uuid.uuid4())
    user_id = Column(UUID(as_uuid=True), default=uuid.uuid4(), nullable=False)
    description = Column(String, nullable=False)
    reply_to = Column(UUID(as_uuid=True), nullable=True)

    post = relationship(
        "PostModel",
        back_populates="comment",
        lazy="selectin"
    )

    def __init__(
            self,
            id_: uuid,
            post_id: uuid,
            user_id: uuid,
            description: str,
            reply_to: uuid

    ):
        self.id = id_
        self.post_id = post_id
        self.user_id = user_id
        self.description = description
        self.reply_to = reply_to

    @classmethod
    def create(
            cls,
            post_id: uuid,
            user_id: uuid,
            description: str,
            reply_to: uuid
    ) -> Union[Comment, NoReturn]:
        id_ = uuid.uuid4()
        return cls(
            id_=id_,
            post_id=post_id,
            user_id=user_id,
            description=description,
            reply_to=reply_to
        )
