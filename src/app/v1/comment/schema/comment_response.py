from pydantic import BaseModel, Field
from pydantic.schema import UUID, Optional, datetime


class CreateCommentResponse(BaseModel):
    id: UUID
    post_id: UUID = Field(alias="postId")
    user_id: UUID = Field(alias="userId")
    description: str
    reply_to: Optional[UUID] = Field(alias="replyTo")
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True


class ReadCommentResponse(BaseModel):
    id: UUID
    post_id: UUID = Field(alias="postId")
    user_id: UUID = Field(alias="userId")
    description: str
    reply_to: UUID = Field(alias="replyTo")
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True


class UpdateCommentResponse(BaseModel):
    id: UUID
    post_id: UUID = Field(alias="postId")
    user_id: UUID = Field(alias="userId")
    description: str
    reply_to: UUID = Field(alias="replyTo")
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True
