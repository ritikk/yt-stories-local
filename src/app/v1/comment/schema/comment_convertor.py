from fastapi_utils.api_model import APIModel
from pydantic.schema import UUID, List, datetime, Optional


class Comment(APIModel):
    id: UUID
    post_id: UUID
    user_id: UUID
    description: str
    reply_to: Optional[UUID]
    created_at: datetime
    updated_at: datetime
