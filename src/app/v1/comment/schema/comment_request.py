from pydantic import BaseModel, Field
from pydantic.schema import UUID, Optional


class CreateCommentRequest(BaseModel):
    description: str
    reply_to: Optional[UUID]


class UpdateCommentRequest(BaseModel):
    post_id: Optional[UUID]
    user_id: Optional[UUID]
    description: Optional[str]
    reply_to: Optional[UUID]


class DeleteCommentRequest(BaseModel):
    id_: Optional[UUID] = Field(alias="id")
