from datetime import datetime
from typing import List, NoReturn, Optional, Union

from pydantic.schema import UUID
from pythondi import inject

from core import CommandService, NoValidData, Propagation, Transactional

from ..models import CommentModel
from ..repository import CommentRepo
from ..schema import Comment


class CommentCommandService(CommandService):

    @inject(
        comment_repo=CommentRepo,
    )
    def __init__(
            self,
            comment_repo: CommentRepo,
    ):
        self.comment_repo = comment_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create(
            self,
            post_id: UUID,
            description: str,
            reply_to: UUID,
            payload: dict
    ) -> Union[Comment, NoReturn]:

        comment = CommentModel.create(
            post_id=post_id,
            user_id=payload.get('id'),
            description=description,
            reply_to=reply_to,
        )

        comment.created_at = datetime.utcnow()
        comment.updated_at = datetime.utcnow()

        await self.comment_repo.save(
            comment=comment,
        )

        return Comment.from_orm(comment)

    async def read(
            self,
            id_: Optional[UUID] = None,
            all_: Optional[bool] = None
    ) -> Union[Comment, NoReturn]:

        if id_:
            comment = await self.comment_repo.get_by_id(
                id_=[id_]
            )
            if not comment:
                print('not found')
            else:
                comment = comment[0]

        elif all_:
            comments = list()
            comment = await self.comment_repo.get_all()
            # print(comment)
            for count in comment:
                comments.append(Comment.from_orm(count))
            # print(comments)
            return comments

        else:
            raise NoValidData(id=id_, all=all_)

        return Comment.from_orm(comment)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update(
            self,
            id_: UUID,
            post_id: UUID,
            user_id: UUID,
            description: str,
            reply_to: UUID

    ) -> Union[Comment, NoReturn]:

        comment = await self.comment_repo.get_by_id(
            id_=[id_]
        )

        if not comment:
            if id_:
                print('comment not found')

        comment = comment[0]

        if post_id:
            comment.post_id = post_id

        if user_id:
            comment.user_id = user_id

        if description:
            comment.description = description

        if reply_to:
            comment.reply_to = reply_to

        comment.updated_at = datetime.utcnow()

        return Comment.from_orm(comment)

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete(
            self,
            id_: UUID
    ) -> None:

        if id_:
            comment = await self.comment_repo.get_by_id(id_=id_)
            if not comment:
                print('comment not found')

            comment = comment[0]
            await self.comment_repo.delete(comment=comment)

            return None

        else:
            raise NoValidData(id=id_)
