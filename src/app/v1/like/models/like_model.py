import uuid
from typing import NoReturn, Union

from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from core import Base

from ..schema import Like


class LikeModel(Base):
    __tablename__ = "like"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4())
    user_id = Column(UUID(as_uuid=True), default=uuid.uuid4())
    post_id = Column(UUID(as_uuid=True), ForeignKey("post.id"), default=uuid.uuid4())

    post = relationship(
        "PostModel",
        back_populates="like",
        lazy="selectin"
    )

    def __init__(
            self,
            id_: uuid,
            user_id: uuid,
            post_id: uuid,
    ):
        self.id = id_
        self.user_id = user_id
        self.post_id = post_id

    @classmethod
    def create(
            cls,
            user_id: uuid,
            post_id: uuid,
    ) -> Union[Like, NoReturn]:
        id_ = uuid.uuid4()
        return cls(
            id_=id_,
            user_id=user_id,
            post_id=post_id,
        )
