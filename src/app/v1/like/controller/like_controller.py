from typing import NoReturn, Union

from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID

from core import ExceptionResponseSchema, TokenHelper

from ..schema import CreateLikeRequest, CreateLikeResponse, Like

from ..service import LikeCommandService

router = InferringRouter()


@cbv(router=router)
class LikeController:

    @router.post(
        "/post/{post_id}/like",
        response_model=CreateLikeResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Like a Post"
    )
    async def create_like(
            self, post_id: UUID, payload: dict = Depends(dependency=TokenHelper())
    ) -> Union[Like, NoReturn]:
        print(payload)
        return await LikeCommandService().create(post_id=post_id, payload=payload)

    @router.post(
        "/post/{post_id}/unlike",
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Unlike a Post"
    )
    async def delete_like(
            self, post_id: UUID
    ) -> None:
        return await LikeCommandService().delete(id_=post_id)
