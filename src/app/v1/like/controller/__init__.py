from .like_controller import LikeController
from .like_controller import router as like_router

__all__ = [
    "like_router",
    "LikeController",
]
