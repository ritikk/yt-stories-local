from .like_exceptions import DuplicateLike

__all__ = [
    "DuplicateLike",
]
