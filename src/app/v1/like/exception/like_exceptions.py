from core import CustomException


class DuplicateLike(CustomException):
    code = 400
    error_code = 20000
    message = "Like already exists"

    def __init__(self, like=None):
        if like:
            self.message = f"{like}: Like already exists."
