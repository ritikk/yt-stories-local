import datetime
from typing import List, Union

from pydantic.schema import UUID
from sqlalchemy import select

from core import Repo, session

from ..models import LikeModel


class LikeRepo(Repo):

    async def save(
            self,
            like: LikeModel,
    ) -> LikeModel:
        session.add(like)

        return like

    async def delete(
            self, like: LikeModel
    ) -> None:
        await session.delete(like)

        return None

    async def get_by_id(
            self, id_: UUID
    ) -> Union[List[LikeModel], None]:
        print(id_)
        query = await session.execute(
            select(LikeModel).filter(
                LikeModel.post_id == id_
            )
        )

        return query.scalars().all()

    async def get_all(self) -> List[LikeModel]:
        pass
