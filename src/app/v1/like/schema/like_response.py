from pydantic import BaseModel, Field
from pydantic.schema import UUID


class CreateLikeResponse(BaseModel):
    id: UUID
    user_id: UUID = Field(alias="userId")
    post_id: UUID = Field(alias="postId")

    class Config:
        orm_mode = True


class ReadLikeResponse(BaseModel):
    id: UUID
    user_id: UUID = Field(alias="userId")
    post_id: UUID = Field(alias="postId")

    class Config:
        orm_mode = True


class UpdateLikeResponse(BaseModel):
    id: UUID
    user_id: UUID = Field(alias="userId")
    post_id: UUID = Field(alias="postId")

    class Config:
        orm_mode = True
