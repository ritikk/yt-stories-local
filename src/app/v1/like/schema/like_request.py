from pydantic import BaseModel, Field
from pydantic.schema import UUID, Optional


class CreateLikeRequest(BaseModel):
    # user_id: UUID
    # post_id: UUID
    pass

class UpdateLikeRequest(BaseModel):
    user_id: Optional[UUID]
    post_id: Optional[UUID]


class DeleteLikeRequest(BaseModel):
    id_: Optional[UUID] = Field(alias="id")
