from fastapi_utils.api_model import APIModel
from pydantic.schema import UUID


class Like(APIModel):
    id: UUID
    user_id: UUID
    post_id: UUID
