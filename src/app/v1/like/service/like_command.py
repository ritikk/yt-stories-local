from typing import NoReturn, Optional, Union

from pydantic.schema import UUID
from pythondi import inject

from core import CommandService, NoValidData, Propagation, Transactional

from ..models import LikeModel
from ..repository import LikeRepo
from ..schema import Like


class LikeCommandService(CommandService):

    @inject(
        like_repo=LikeRepo,
    )
    def __init__(
            self,
            like_repo: LikeRepo,
    ):
        self.like_repo = like_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create(
            self,
            post_id: UUID,
            payload: dict
    ) -> Union[Like, NoReturn]:

        like = LikeModel.create(
            user_id=payload.get('id'),
            post_id=post_id,
        )

        await self.like_repo.save(
            like=like,
        )

        return Like.from_orm(like)

    async def read(
            self,
            id_: Optional[UUID] = None,
            all_: Optional[bool] = None
    ) -> Union[Like, NoReturn]:

        pass

    @Transactional(propagation=Propagation.REQUIRED)
    async def update(
            self,
            id_: UUID,
            user_id: UUID,
            post_id: UUID,
    ) -> Union[Like, NoReturn]:

        pass

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete(
            self,
            id_: UUID
    ) -> None:

        if id_:
            like = await self.like_repo.get_by_id(id_=id_)
            print(like)
            if not like:
                print('post not found')

            like = like[0]
            await self.like_repo.delete(like=like)

            return None

        else:
            raise NoValidData(id=id_)
