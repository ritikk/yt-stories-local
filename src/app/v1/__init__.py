from fastapi import APIRouter

from core import Base

from .post import post_router
from .like import like_router
from .comment import comment_router
from .report import report_router

v1_router = APIRouter()

v1_router.include_router(
    post_router,
    prefix="/post",
    tags=["Post"]
)
v1_router.include_router(
    like_router,
    # prefix="/like",
    tags=["Like"]
)
v1_router.include_router(
    comment_router,
    # prefix="/comment",
    tags=["Comment"]
)
v1_router.include_router(
    report_router,
    # prefix="/report",
    tags=["Report"]
)

__all__ = ["v1_router", "Base"]
