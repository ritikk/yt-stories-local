from fastapi_utils.api_model import APIModel
from pydantic.schema import UUID, datetime


class Post(APIModel):
    id: UUID
    media_path: str
    user_id: UUID
    title: str
    description: str
    status: int
    created_at: datetime
    updated_at: datetime
