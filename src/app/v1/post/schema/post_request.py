from enum import Enum

from pydantic import BaseModel, Field
from pydantic.schema import UUID, Optional


class CreatePostRequest(BaseModel):
    media_path: str
    title: str
    description: str
    status: int


class UpdatePostRequest(BaseModel):
    user_id: Optional[UUID]
    media_path: Optional[str]
    title: Optional[str]
    description: Optional[str]
    status: Optional[int]


class DeletePostRequest(BaseModel):
    id_: Optional[UUID] = Field(alias="id")
