from pydantic import BaseModel, Field
from pydantic.schema import UUID, datetime


class CreatePostResponse(BaseModel):
    id: UUID
    user_id: UUID = Field(alias="userId")
    media_path: str = Field(alias="mediaPath")
    title: str = Field(max_length=45)
    description: str = Field(max_length=160)
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True


class ReadPostResponse(BaseModel):
    id: UUID
    media_path: str = Field(alias="mediaPath")
    user_id: UUID = Field(alias="userId")
    title: str
    description: str
    status: int
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True


class UpdatePostResponse(BaseModel):
    id: UUID
    media_path: str = Field(alias="mediaPath")
    user_id: UUID = Field(alias="userId")
    title: str
    description: str
    status: int
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True
