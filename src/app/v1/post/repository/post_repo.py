from typing import List, Union

from pydantic.schema import UUID
from sqlalchemy import select

from core import Repo, session

from ..models import PostModel


class PostRepo(Repo):

    async def save(
            self,
            post: PostModel,
    ) -> PostModel:
        session.add(post)

        return post

    async def delete(
            self, post: PostModel
    ) -> None:
        await session.delete(post)

        return None

    async def get_by_id(
            self, id_: List[UUID]
    ) -> Union[List[PostModel], None]:
        query = await session.execute(
            select(PostModel).where(
                PostModel.id.in_(id_)
            )
        )

        return query.scalars().all()

    async def get_all(self) -> List[PostModel]:
        query = await session.execute(
            select(PostModel)
        )

        return query.scalars().all()
