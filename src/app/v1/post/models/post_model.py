import uuid
from typing import NoReturn, Union

from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

from core import Base, TimestampMixin

from ..schema import Post


class PostModel(Base, TimestampMixin):
    __tablename__ = "post"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4())
    media_path = Column(String, nullable=False)
    user_id = Column(UUID(as_uuid=True), default=uuid.uuid4(), nullable=False)
    title = Column(String, nullable=False)
    description = Column(String, nullable=False)
    status = Column(Integer, nullable=False)

    like = relationship(
        "LikeModel",
        back_populates="post",
        lazy="selectin"
    )

    comment = relationship(
        "CommentModel",
        back_populates="post",
        lazy="selectin"
    )

    report = relationship(
        "ReportModel",
        back_populates="post",
        lazy="selectin"
    )

    def __init__(
            self,
            id_: uuid,
            media_path: str,
            user_id: uuid,
            title: str,
            description: str,
            status: int

    ):
        self.id = id_
        self.media_path = media_path
        self.user_id = user_id
        self.title = title
        self.description = description
        self.status = status

    @classmethod
    def create(
            cls,
            media_path: str,
            user_id: uuid,
            title: str,
            description: str,
            status: int,
    ) -> Union[Post, NoReturn]:
        id_ = uuid.uuid4()
        return cls(
            id_=id_,
            media_path=media_path,
            user_id=user_id,
            title=title,
            description=description,
            status=status
        )
