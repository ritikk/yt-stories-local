from datetime import datetime
from typing import NoReturn, Optional, Union
from pydantic.schema import UUID
from pythondi import inject

from core import CommandService, NoValidData, Propagation, Transactional

from ..models import PostModel
from ..repository import PostRepo
from ..schema import Post
from ..exception import PostNotFound


class PostCommandService(CommandService):

    @inject(
        post_repo=PostRepo,
    )
    def __init__(
            self,
            post_repo: PostRepo,
    ):
        self.post_repo = post_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create(
            self,
            media_path: str,
            title: str,
            description: str,
            status: int,
            payload: dict
    ) -> Union[Post, NoReturn]:

        post = PostModel.create(
            media_path=media_path,
            user_id=payload.get("id"),
            title=title,
            description=description,
            status=status,
        )

        post.created_at = datetime.utcnow()
        post.updated_at = datetime.utcnow()

        await self.post_repo.save(
            post=post,
        )

        return Post.from_orm(post)

    async def read(
            self,
            id_: Optional[UUID] = None,
            all_: Optional[bool] = None
    ) -> Union[Post, NoReturn]:

        if id_:
            post = await self.post_repo.get_by_id(
                id_=[id_]
            )
            if not post:
                print('not found')
            else:
                post = post[0]

        elif all_:
            posts = list()
            post = await self.post_repo.get_all()
            # print(post)
            for count in post:
                # print(post)
                posts.append(Post.from_orm(count))

            return posts

        else:
            raise NoValidData(id=id_, all=all_)

        return Post.from_orm(post)

    @Transactional(propagation=Propagation.REQUIRED)
    async def update(
            self,
            id_: UUID,
            media_path: str,
            user_id: UUID,
            title: str,
            description: str,
            status: int

    ) -> Union[Post, NoReturn]:

        post = await self.post_repo.get_by_id(
            id_=[id_]
        )

        if not post:
            if id_:
                raise PostNotFound

        post = post[0]
        if media_path:
            post.media_path = media_path

        if user_id:
            post.user_id = user_id

        if title:
            post.title = title

        if description:
            post.description = description

        if status:
            post.status = status

        post.updated_at = datetime.utcnow()

        return Post.from_orm(post)

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete(
            self,
            id_: UUID
    ) -> None:

        if id_:
            post = await self.post_repo.get_by_id(id_=[id_])
            if not post:
                raise PostNotFound

            post = post[0]
            await self.post_repo.delete(post=post)

            return None

        else:
            raise NoValidData(id=id_)
