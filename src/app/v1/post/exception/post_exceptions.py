from core import CustomException


class PostNotFound(CustomException):
    code = 404
    error_code = 10000
    message = "Post doesn't exist"

    def __init__(self, post=None):
        if post:
            self.message = f"{post}: Post doesn't exist."


class EmptyPost(CustomException):
    code = 400
    error_code = 30000
    message = "No post found in the database. Please create post first"
