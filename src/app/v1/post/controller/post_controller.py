from typing import List, NoReturn, Union

from fastapi import Depends, Header
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID

from core import ExceptionResponseSchema, TokenHelper

from ..schema import (CreatePostRequest, CreatePostResponse, Post,
                      ReadPostResponse, UpdatePostRequest,
                      UpdatePostResponse)
from ..service import PostCommandService

router = InferringRouter()


@cbv(router=router)
class PostController:

    @router.post(
        "/",
        response_model=CreatePostResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Create Post"
    )
    async def create_post(
            self, request: CreatePostRequest, payload: dict = Depends(dependency=TokenHelper())
    ) -> Union[Post, NoReturn]:
        payload_id = payload.get("id")
        print(payload_id)
        print(payload)
        return await PostCommandService().create(payload=payload, **request.dict())

    @router.put(
        "/{post_id}",
        include_in_schema=False,
        response_model=UpdatePostResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Update Post"
    )
    async def update_post(
            self,
            request: UpdatePostRequest, post_id: UUID
    ) -> Union[Post, NoReturn]:
        return await PostCommandService(). \
            update(post_id, **request.dict())

    @router.get(
        "/{post_id}",
        include_in_schema=False,
        response_model=ReadPostResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get a Post"
    )
    async def get_post(
            self, post_id: UUID
    ) -> Union[Post, NoReturn]:
        return await PostCommandService().read(id_=post_id)

    @router.get(
        "/",
        response_model=List[ReadPostResponse],
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Get all Posts"
    )
    async def get_all_posts(
            self,
    ) -> Union[List[Post], NoReturn]:
        return await PostCommandService().read(all_=True)

    @router.delete(
        "/{post_id}",
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Delete a Post"
    )
    async def delete_post(
            self, post_id: UUID
    ) -> None:
        return await PostCommandService().delete(id_=post_id)
