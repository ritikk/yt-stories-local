from .post_controller import PostController
from .post_controller import router as post_router

__all__ = [
    "post_router",
    "PostController",
]
