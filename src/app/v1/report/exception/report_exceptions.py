from core import CustomException


class DuplicateReport(CustomException):
    code = 400
    error_code = 20000
    message = "Report already exists"

    def __init__(self, report=None):
        if report:
            self.message = f"{report}: Report already exists."
