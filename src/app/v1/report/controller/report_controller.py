from typing import NoReturn, Union

from fastapi import Depends
from fastapi_utils.cbv import cbv
from fastapi_utils.inferring_router import InferringRouter
from pydantic.schema import UUID

from core import ExceptionResponseSchema, TokenHelper

from ..schema import (CreateReportRequest, CreateReportResponse, Report)
from ..service import ReportCommandService

router = InferringRouter()


@cbv(router=router)
class ReportController:

    @router.post(
        "/{post_id}/report",
        response_model=CreateReportResponse,
        responses={"400": {"model": ExceptionResponseSchema}},
        summary="Report a Post"
    )
    async def create_report(
            self, request: CreateReportRequest, post_id: UUID, payload: dict = Depends(dependency=TokenHelper())
    ) -> Union[Report, NoReturn]:
        print(payload)
        return await ReportCommandService().create(payload=payload, post_id=post_id, **request.dict())
