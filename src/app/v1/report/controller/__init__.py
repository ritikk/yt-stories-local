from .report_controller import ReportController
from .report_controller import router as report_router

__all__ = [
    "report_router",
    "ReportController",
]
