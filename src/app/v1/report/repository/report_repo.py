import datetime
from typing import List, Union

from pydantic.schema import UUID
from sqlalchemy import select

from core import Repo, session

from ..models import ReportModel


class ReportRepo(Repo):

    async def save(
            self,
            report: ReportModel,
    ) -> ReportModel:
        session.add(report)

        return report

    async def delete(
            self, report: ReportModel
    ) -> None:
        pass

    async def get_by_id(
            self, id_: List[UUID]
    ) -> Union[List[ReportModel], None]:
        pass

    async def get_all(self) -> List[ReportModel]:
        pass
