from pydantic import BaseModel, Field
from pydantic.schema import UUID, Optional, datetime


class CreateReportResponse(BaseModel):
    id: UUID
    post_id: UUID = Field(alias="postId")
    report_by: UUID = Field(alias="reportBy")
    report_reason: str = Field(alias="reportReason")
    reason_description: Optional[str] = Field(alias="reason_description")
    created_at: datetime = Field(alias="createdAt")
    updated_at: datetime = Field(alias="updatedAt")

    class Config:
        orm_mode = True
