from enum import Enum

from pydantic import BaseModel
from pydantic.schema import UUID, Optional


class ReportReason(str, Enum):
    sexual_content = "Sexual content"
    violent_or_repulsive_content = "Violent or repulsive content"
    hateful_or_abusive_content = "Hateful or abusive content"
    harmful_or_dangerous_acts = "Harmful or dangerous acts"


class CreateReportRequest(BaseModel):
    report_reason: str
    reason_description: Optional[str]
