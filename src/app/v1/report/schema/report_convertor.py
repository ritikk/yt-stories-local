from fastapi_utils.api_model import APIModel
from pydantic.schema import UUID, datetime, Optional

from .report_request import ReportReason


class Report(APIModel):
    id: UUID
    post_id: UUID
    report_by: UUID
    report_reason: str
    reason_description: Optional[str]
    created_at: datetime
    updated_at: datetime
