from datetime import datetime
from typing import List, NoReturn, Optional, Union

from pydantic.schema import UUID
from pythondi import inject

from core import CommandService, Propagation, Transactional

from ..models import ReportModel
from ..repository import ReportRepo
from ..schema import Report


class ReportCommandService(CommandService):

    @inject(
        report_repo=ReportRepo,
    )
    def __init__(
            self,
            report_repo: ReportRepo,
    ):
        self.report_repo = report_repo

    @Transactional(propagation=Propagation.REQUIRED)
    async def create(
            self,
            post_id: UUID,
            report_reason: str,
            reason_description: str,
            payload: dict
    ) -> Union[Report, NoReturn]:
        report = ReportModel.create(
            post_id=post_id,
            report_by=payload.get("id"),
            report_reason=report_reason,
            reason_description=reason_description,
        )

        report.created_at = datetime.utcnow()
        report.updated_at = datetime.utcnow()

        await self.report_repo.save(
            report=report,
        )

        return Report.from_orm(report)

    async def read(
            self,
            id_: Optional[UUID] = None,
            all_: Optional[bool] = None
    ) -> Union[Report, NoReturn]:
        pass

    @Transactional(propagation=Propagation.REQUIRED)
    async def update(
            self,
            id_: UUID,
            post_id: UUID,
            report_by: UUID,
            reason_description: str,

    ) -> Union[Report, NoReturn]:
        pass

    @Transactional(propagation=Propagation.REQUIRED)
    async def delete(
            self,
            id_: UUID
    ) -> None:
        pass
