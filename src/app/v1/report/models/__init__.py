from .report_model import ReportModel

__all__ = [
    "ReportModel"
]
