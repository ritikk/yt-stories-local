import uuid
from typing import NoReturn, Union

from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from ..schema.report_request import ReportReason
from sqlalchemy_utils.types import ChoiceType

from core import Base, TimestampMixin

from ..schema import Report


class ReportModel(Base, TimestampMixin):
    __tablename__ = "report"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4())
    post_id = Column(UUID(as_uuid=True), ForeignKey("post.id"), default=uuid.uuid4())
    report_by = Column(UUID(as_uuid=True), default=uuid.uuid4(), nullable=False)
    report_reason = Column(ChoiceType(ReportReason, impl=String()))
    reason_description = Column(String, nullable=True)

    post = relationship(
        "PostModel",
        back_populates="report",
        lazy="selectin"
    )

    def __init__(
            self,
            id_: uuid,
            post_id: uuid,
            report_by: uuid,
            report_reason: str,
            reason_description: str,

    ):
        self.id = id_
        self.post_id = post_id
        self.report_by = report_by
        self.report_reason = report_reason
        self.reason_description = reason_description

    @classmethod
    def create(
            cls,
            post_id: uuid,
            report_by: uuid,
            report_reason: str,
            reason_description: str,
    ) -> Union[Report, NoReturn]:
        id_ = uuid.uuid4()
        return cls(
            id_=id_,
            post_id=post_id,
            report_by=report_by,
            report_reason=report_reason,
            reason_description=reason_description,
        )
