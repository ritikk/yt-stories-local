from fastapi import Depends, FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from pydantic import ValidationError
from pythondi import Provider, configure

from app import router
from config import AppEnvironment, config
from core import (Cache, CustomException, CustomKeyMaker, Logging,
                  RedisBackend, Repo, ResponseMiddleware, SQLAlchemyMiddleware)


from .v1.post import PostRepo
from .v1.like import LikeRepo
from .v1.comment import CommentRepo
from .v1.report import ReportRepo


def init_cors(_app: FastAPI) -> None:
    _app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


def init_routers(_app: FastAPI) -> None:
    _app.include_router(router)


def init_listeners(_app: FastAPI) -> None:
    @_app.exception_handler(CustomException)
    async def custom_exception_handler(request: Request, exc: CustomException):
        return JSONResponse(
            status_code=exc.code,
            content={"error_code": exc.error_code, "message": exc.message},
        )

    @_app.exception_handler(ValidationError)
    async def validation_error_handler(request: Request, exc: ValidationError):
        return JSONResponse(
            status_code=400,
            content={"error_code": 400, "message": exc.errors()},
        )

    @_app.exception_handler(Exception)
    async def error_handler(request: Request, exc: Exception):
        return JSONResponse(
            status_code=500,
            content={"error_code": 500, "message": exc.args},
        )


def on_auth_error(request: Request, exc: Exception):
    status_code, error_code, message = 401, None, str(exc)
    if isinstance(exc, CustomException):
        status_code = int(exc.code)
        error_code = exc.error_code
        message = exc.message

    return JSONResponse(
        status_code=status_code,
        content={"error_code": error_code, "message": message},
    )


def init_middleware(_app: FastAPI) -> None:
    _app.add_middleware(SQLAlchemyMiddleware)
    _app.add_middleware(ResponseMiddleware)


def init_cache() -> None:
    Cache.init(backend=RedisBackend(), key_maker=CustomKeyMaker())


def init_di() -> None:
    provider = Provider()
    provider.bind(Repo, PostRepo)
    provider.bind(Repo, LikeRepo)
    provider.bind(Repo, CommentRepo)
    provider.bind(Repo, ReportRepo)
    configure(provider=provider)


def create_app() -> FastAPI:
    _app = FastAPI(
        title="Hide",
        description="Hide API",
        version="1.0.0",
        docs_url=None if config.ENV == AppEnvironment.Production else "/docs",
        redoc_url=None if config.ENV == AppEnvironment.Production
        else "/redoc",
        dependencies=[Depends(Logging)],
    )
    init_routers(_app=_app)
    init_cors(_app=_app)
    init_listeners(_app=_app)
    init_middleware(_app=_app)
    init_cache()
    init_di()
    return _app


app = create_app()
